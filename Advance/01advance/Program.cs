﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01advance
{
    class Program
    {
        static void Main(string[] args)
        {
            // 集合的大小是 可以 动态变化的 
            // 但是 集合 只能用于 一维数据的 存储

            #region 1. 分拣奇偶数。将字符串"1 2 3 4 5 6 7 8 9 10"中的数据按照“奇数在前、偶数在后”的格式进行调整。（提示：List<T>）

            string str1 = "1 2 3 4 5 6 7 8 9 10";
            List<string> oddList = new List<string>();
            List<string> evenList = new List<string>();
            string[] nums = str1.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("处理前：" + str1);

            foreach (string item in nums)
            {
                if (int.Parse(item) % 2 != 0)
                {
                    oddList.Add(item);
                }
                else
                {
                    evenList.Add(item);
                }

            }
            oddList.AddRange(evenList);
            string result = string.Join(" ", oddList);  // 在每个元素之间 加入 指定的分隔符
            Console.WriteLine("处理后：" + result);
            #endregion
            Console.WriteLine("********************************************************");
            
            #region 2. 某int集合中有10个不重复的随机整数，将其中的奇数挑选出来，保存在一个int数组中。（提示：List<T>）
            List<int> list1 = new List<int>();
            Random rd = new Random();
            while(list1.Count < 10)
            {
                int num = rd.Next(1, 100);
                if (!list1.Contains(num))
                {
                    list1.Add(num);
                }
            }
            // 由于不知道 奇数有多少个，所以用集合来处理
            List<int> oddList2 = new List<int>();
            int[] oddArr = list1.Where(x => x % 2 == 1).ToArray();

            Console.WriteLine($"其中有{oddArr.Length}个奇数，分别是：");
            foreach (int item in oddArr)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            #endregion
            Console.WriteLine("********************************************************");

            #region 3.接收用户输入的“123”，输出“壹贰叁”。(提示：Dictionary<K,V>  "0零 1壹 2贰 3叁 4肆 5伍 6陆 7柒 8捌 9玖")

            string msg = "0零 1壹 2贰 3叁 4肆 5伍 6陆 7柒 8捌 9玖";
            Dictionary<int, char> dic1 = new Dictionary<int, char>();
            string[] parts = msg.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in parts)
            {
                // item："0壹"
                dic1.Add(item[0], item[1]);
            }
            Console.WriteLine("请输入整数：");
            string numbers = Console.ReadLine();

            StringBuilder stringBuilder = new StringBuilder();

            foreach (char item in numbers)
            {
                stringBuilder.Append(dic1[item]);
            }
            Console.WriteLine("直接输出stringBuilder：" + stringBuilder);
            string result2 = stringBuilder.ToString();
            Console.WriteLine("先转换为string类型，再输出：" + result2);
            #endregion
            Console.WriteLine("********************************************************");

            #region 4. 计算字符串中每种字母出现的次数。"Hello! Welcome to China!  How are you ?  Fine!  And you?"（扩展：不区分大小写）(提示：Dictionary<K,V>)

            string str2 = "Hello! Welcome to China!  How are you ?  Fine!  And you?";
            Dictionary<char, int> dic2 = new Dictionary<char, int>();
            str2 = str2.ToLower();  // 如果区分大小写，就注释这条语句
            // 也可以在后面，遍历每一个字符的时候，去判断它的大写和小写形式是不是在里面

            foreach (char item in str2)
            {
                if (char.IsLetter(item))    // 判断是不是字母，包括大小写
                {
                    if (dic2.ContainsKey(item))
                    {
                        dic2[item]++;
                    }
                    else
                    {
                        //dic2[item] = 1;
                        dic2.Add(item, 1);
                    }
                }
            }
            foreach (KeyValuePair<char,int> item in dic2)
            {
                Console.WriteLine($"字母{item.Key}出现了{item.Value}次");
            }
            #endregion
            Console.WriteLine("********************************************************");


            #region 5. 设计方法实现，将中文日期转换为数字格式的日期。比如：“二零一九年九月二十日”应转换为“2019-9-20”。（"零0 一1 二2 三3 四4 五5 六6 七7 八8 九9"， 难点：“十”的转换）




            #endregion


            Console.ReadKey();

        }
    }
}
