﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01base
{
    class Program
    {
        /*
         * 8.有如下字符串："患者：“大夫，我咳嗽得很重。”     大夫：“你多大年记？”     患者：“七十五岁。”     大夫：“二十岁咳嗽吗”患者：“不咳嗽。”     大夫：“四十岁时咳嗽吗？”     患者：“也不咳嗽。”     大夫：“那现在不咳嗽，还要等到什么时咳嗽？”"。需求：请统计出该字符中“咳嗽”二字的出现次数，以及每次“咳嗽”出现的索引位置。
         * 
         * 9.从日期字符串"2020年10月1日"中把年月日的数据分别取出来，打印到控制台
         */
        static void Main(string[] args)
        {
            //string str1 = "患者：“大夫，我咳嗽得很重。”     大夫：“你多大年记？”     患者：“七十五岁。”     大夫：“二十岁咳嗽吗”患者：“不咳嗽。”     大夫：“四十岁时咳嗽吗？”     患者：“也不咳嗽。”     大夫：“那现在不咳嗽，还要等到什么时咳嗽？”";

            ////IndexOf 有九个重载的方法
            //int count = 0;
            //int index = str1.IndexOf("咳嗽", 0);  // 从索引为 0 的位置 开始 搜索

            //while (index != -1)
            //{
            //    count++;
            //    Console.WriteLine($"第{count}次出现“咳嗽”的索引位置: {index}");
            //    index += "咳嗽".Length;
            //    index = str1.IndexOf("咳嗽", index);
            //}
            //Console.ReadKey();

            #region 9.从日期字符串"2020年10月1日"中把年月日的数据分别取出来，打印到控制台

            //string str2 = "2020年10月1日";
            //char[] charArr = new char[] { '年', '月', '日' };
            //string[] parts = str2.Split(charArr, StringSplitOptions.RemoveEmptyEntries); // 不要空格字符
            //Console.WriteLine("日期数组为：" + string.Join(", ", parts));

            //foreach (string item in parts)
            //{
            //    Console.WriteLine("单个日期为：" + item);
            //}
            //Console.ReadKey();
            //#endregion

            //#region 测试GetUpperBound()  和  GerLowerBound() 函数

            // 定义一个 两行四列的数组arr
            /*
             * arr = {
             *  { 1, 3, 5, 7 },
             *  { 2, 4, 6, 8 }
             * }
             */
            int[,] arr = new int[2, 4] { { 1, 3, 5, 7 }, { 2, 4, 6, 8 } };

            Console.WriteLine(arr.GetUpperBound(0));    // 1
            Console.WriteLine(arr.GetLowerBound(0));    // 0
            Console.WriteLine(arr.GetUpperBound(1));    // 3
            Console.WriteLine(arr.GetLowerBound(1));    // 0
            Console.ReadKey();

            #endregion

        }
    }
}
