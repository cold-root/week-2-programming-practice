﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11集合练习题
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 1. 分拣奇偶数。将字符串"1 2 3 4 5 6 7 8 9 10"中的数据按照“奇数在前、偶数在后”的格式进行调整。（提示：List<T>）

            string str1 = "1 2 3 4 5 6 7 8 9 10";
            string[] strs = str1.Split(' ');
            //int[] arr1 = new int[10];
            //int[] arr1 = Array.ConvertAll(strs, int.Parse); // 将字符串数组 转成 整型数组
            //List<int> list1 = new List<int>();
            //list1.AddRange(arr1);

            List<int> list1 = Array.ConvertAll(strs, int.Parse).ToList();

            Console.WriteLine("处理前：" + str1);
            // 先获取所有的奇数
            int[] arr2 = list1.Where(x => x % 2 == 1).ToArray();
            // 再获取所有的偶数
            int[] arr3 = list1.Where(x => x % 2 == 0).ToArray();

            List<int> list2 = new List<int>();
            list2.AddRange(arr2);
            list2.AddRange(arr3);
            //Console.WriteLine("处理后：");
            //foreach (int item in list2)
            //{
            //    Console.Write(item + " ");
            //}

            string result = string.Join(" ", list2);  // 在每个元素之间 加入 指定的分隔符
            Console.WriteLine("处理后：" + result);
            #endregion
            Console.WriteLine("*************************************************************");
            
            #region 2.某int集合中有10个不重复的随机整数，将其中的奇数挑选出来，保存在一个int数组中。（提示：List<T>）
            List<int> list3 = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                Random rd = new Random();
                int r1 = rd.Next(1, 100);  // 返回一个 非负 随机整数  产生的数据不会包含100
                
                while(list3.Contains(r1))
                {
                    rd = new Random();
                    r1 = rd.Next(1, 100);
                }
                list3.Add(r1);
            }
            Console.Write("原始数据：");
            foreach (int item in list3)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            int[] arr4 = list3.Where(x => x % 2 == 1).ToArray();
            Console.Write("处理结果存入数组后为：");
            foreach (int item in arr4)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            #endregion
            Console.WriteLine("*************************************************************");

            #region 3.接收用户输入的“123”，输出“壹贰叁”。(提示：Dictionary<K,V>  "0零 1壹 2贰 3叁 4肆 5伍 6陆 7柒 8捌 9玖")
            Dictionary<int, char> dic1 = new Dictionary<int, char>();
            int[] ints = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            char[] chars = new char[10] { '零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖' };

            for (int i = 0; i < 10; i++)
            {
                dic1.Add(ints[i], chars[i]);
            }
            foreach (KeyValuePair<int, char> item in dic1)
            {
                Console.WriteLine("键：{0} => 值：{1}", item.Key, item.Value);
            }

            int[] arr5 = new int[3];
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("请输入三个数字，以回车分隔");
                arr5[i] = Convert.ToInt32(Console.ReadLine());
            }
            // 对应数字 的 中文字符分别为：
            foreach (int item in arr5)
            {
                Console.WriteLine("数字：{0} => 中文：{1}", item, dic1[item]);
            }
            #endregion
            Console.WriteLine("*************************************************************");


            #region 4. 计算字符串中每种字母出现的次数。"Hello! Welcome to China!  How are you ?  Fine!  And you?"（扩展：不区分大小写）(提示：Dictionary<K,V>)
            Console.WriteLine("4. 计算字符串中 每种字母出现的次数");
            string str2 = "Hello! Welcome to China!  How are you ?  Fine!  And you?";

            str2 = str2.ToLower(); // 把字母 都变成小写
            char[] chars2 = str2.ToCharArray(); // 把str2转成 一个 字符数组

            Dictionary<char, int> dic2 = new Dictionary<char, int>();

            foreach (char item in chars2)
            {
                if (char.IsLetter(item))    // 判断是否为小写字母
                {
                    if (dic2.ContainsKey(item))
                    {
                        // 如果键中 包含该字符
                        dic2[item] += 1;    // 对应计数 + 1
                    }
                    else
                    {
                        // 如果不包含该字符
                        dic2[item] = 1; // 添加该字符，计数置为1
                    }
                }
            }
            foreach (KeyValuePair<char, int> item in dic2)//遍历输出
            {
                Console.WriteLine("字母{0}出现的次数是：{1}", item.Key, item.Value);
            }
            #endregion
            Console.WriteLine("*************************************************************");


            #region 5. 设计方法实现，将中文日期转换为数字格式的日期。比如：“二零一九年九月二十日”应转换为“2019-9-20”。（"零0 一1 二2 三3 四4 五5 六6 七7 八8 九9"， 难点：“十”的转换）
            // 十一月二十一日 -> 11-21  





            #endregion

            Console.ReadKey();
        }
    }
}
