﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02面向对象
{
    internal class Person
    {
        // internal声明：这个类可以在当前项目中被访问
        //public string name;
        //public int age;

        private string name;
        private int age;

        // ctrl + R + E 快速生成 相对应数据的 属性代码

        public char Gender { get; set; }    // 自动属性

        // 声明属性
        public string Name
        {
            get { return name; }    // 读操作
            set { name = value; }   // 写操作
        }

        public int Age
        {
            get { return age; }
            set 
            {
                if (value>=0)
                {
                    age = value;
                }
                else
                {
                    Console.WriteLine("输入的年龄有误！");
                    age = 0;
                }
            }
        }

        public void Speak()
        {
            //int age = 80;   // 声明局部变量
            Console.WriteLine("我是{0},我今年{1}岁了", name, this.age);
        }

    }
}
