﻿using _02面向对象;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03类和对象
{
    class Program
    {

        static int m = 3;
        int n = 4;

        static void Main(string[] args)
        {
            Person p1 = new Person();
            //p1.name = "张三";
            //p1.age = 30;

            p1.Name = "张三";
            p1.Age = -18;
            p1.Speak();
            //Console.ReadKey();


            // 通过对属性的访问，可以 间接实现 对私有字段的访问
            // 属性定义包括 属性头 和 属性体
            // 属性头 包括 属性名 等信息
            // 属性体 只包含 get 和 set两个访问器  或 其中之一
            // 其中，get访问器 用于 获取属性的值，也就是 读操作
            // set访问器 设置属性的值 也就是 写操作


            /*
             * 类的常用修饰符：
             * public：表示访问权限不受限制
             * abstract：表示这是一个抽象类
             * sealed：表示这是一个密封类
             * 
             * 类的成员的常用修饰符
             * public:公有，访问不受限制
             * private：表示私有成员，只能在成员所在类内访问。这是默认的
             * protected：表示受保护成员，只能在成员所在类和子类内访问。
             * static：表示静态成员，只能使用类名字访问该成员。
             * 没有static修饰的成员是 实例成员，需要用类的实例（即对象）进行访问。
             */

            m = 5;  // 可以直接访问当前类的静态字段
            new Program().n = 6;    // 需要创建类的对象 来访问

            // 访问其他类的静态字段和实例字段
            A.i = 3;
            A a = new A();
            a.j = 5;
            Console.ReadKey();

        }
    }

    class A
    {
        public static int i = 1;
        public int j = 2;
    }

}
