﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10泛型集合
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 1. List<T>
            List<int> list1 = new List<int>() { 1, 3, 5, 7, 9, 2, 4, 6, 8, 10};

            // 泛型集合 保留了 普通集合的所有方法
            //list1.Add(3.14);  类型不匹配
            list1.Add(10);

            // 泛型集合 还提供了 一些扩展方法
            //Console.WriteLine(list1.Max());
            //Console.WriteLine(list1.Min());
            //Console.WriteLine(list1.Average());
            //Console.WriteLine(list1.Sum());

            // 获取 集合里面的 特征元素
            // 希望能够提取出 集合里面 大于5的元素
            // 得到list1中所有 大于5的数  并组成一个集合
            // x => x > 5 是一个 委托类型参数(要求传递参数类型是int，返回类型是bool)，是一个lamda函数
            List<int> list2 = list1.Where(x => x > 5).ToList();

            foreach (int item in list2)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            // 提取所有的 奇数，并保存到一个 int 数组里面
            int[] arr3 = list1.Where(x => x % 2 == 1).ToArray();
            foreach (int item in arr3)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            // 对集合进行 直接排序
            list1.Sort();
            foreach (int item in list1)
            {
                Console.WriteLine(item);
            }

            #endregion

            #region 2. Dictionary<K,V>
            // 键值对 的集合   案例：<姓名,学号>  的键值对 的集合
            Dictionary<string, int> dic1 = new Dictionary<string, int>();
            dic1.Add("小明", 0001);
            dic1.Add("小红", 0002);

            if (dic1.ContainsKey("小明"))
            {
                Console.WriteLine("键已存在，无法添加");
            }
            else
            {
                dic1.Add("小明", 0001);
            }

            // 添加 或 修改 的 简单操作
            dic1["小蓝"] = 0003;


            // 遍历键
            foreach (string item in dic1.Keys)
            {
                Console.WriteLine("键为：" + item);
            }

            // 遍历值
            foreach (int item in dic1.Values)
            {
                Console.WriteLine("值为：" + item);
            }

            // 遍历键值对
            foreach (KeyValuePair<string, int> item in dic1)
            {
                Console.WriteLine("键：{0} => 值：{1}", item.Key, item.Value);
            }
            #endregion

            Console.ReadKey();
        }
    }
}
