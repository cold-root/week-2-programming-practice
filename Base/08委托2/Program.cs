﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08委托2
{
    class Program
    {
        static void Main(string[] args)
        {
            // 内置委托
            // Action：无参数无返回值的委托
            // Action<T>：有参数无返回值的委托
            // Func<T,T>：有参数有返回值的委托

            // 委托 通常 和 lamda表达式一起用
            Action action01 = new Action(() =>
            {
                Console.WriteLine("无参数无返回值的方法。");
            });
            action01();

            // 这个带参数的 委托，可以接收 1~16个参数
            Action<string> action02 = new Action<string>((str1) =>
            {
                Console.WriteLine("有参数无返回值的方法，参数是{0}", str1);
            });
            action02("Hello");

            Func<int, int> func = new Func<int, int>((n) =>
            {
                int sum = 0;
                for (int i = 0; i < n + 1; i++)
                {
                    sum += i;
                }
                return sum;
            });
            Console.WriteLine(func(100));


            // 使用委托第二步：声明 一个 适合的委托类型的数据 来 关联 方法
            Action ac = func2;      // 可以封装为一个 方法，每次都接收一个 不同的方法，然后声明为一个 委托类型的 变量
            // Action 对应的委托类型 是 无返回值的      是系统自带的一个 已经声明好的委托类型 
            // Action<> // 可以传递参数  可以有1~10个参数
            // Func<>  // 可以带上返回类型 
            // 很少自己定义委托类型，如果要用到委托 就使用自带的 Action 或者 Func即可
            // 使用委托第三步：直接执行：ac()   或者 将其作为一个参数 传递到一个专门执行这个方法的函数中去
            DoSomething(ac);  // 可以接受外界传递过来的方法  然后进行调用这个方法

            // 将所有使用委托的 三个步骤 整合到一个地方来， 用labmda表达式即可
            DoSomething(() => {
                Console.WriteLine("使用labmda表示的方法: 数据写入数据库");   
            });  // labmda表达式，代表一个方法   =>:goes to   左边()代表方法的参数  右边{}代表方法体 
                 // 直接将  方法声明 和Action类型 的 数据声明  都省略掉了 

            //Compute();
            // 下面使用labmda方法 实现WordCount案例
            string msg = "hello world hello C# python C#";

            string[] parts = msg.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in parts)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            // GroupBy: 根据指定的 键选择器函数（Select） 对序列的元素 进行分组
            // Select：将序列中的每个元素 投影到 新表单
            var enumerable = parts.GroupBy(str=>str)
                .Select(g => new { Key = g.Key, Count = g.Count() });

            foreach (var item in enumerable)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }

        private static void DoSomething(Action ac)
        {
            ac();
        }

        //private static void Compute(Func<int, int> func)
        //{
        //    func(i); // ??? 有点小问题
        //}

        // 使用委托第一步：声明 有参数的方法
        static void func1()
        {
            Console.WriteLine("数据写入文件方法 func1");
        }

        static void func2()
        {
            Console.WriteLine("数据写入数据库方法 func2");
        }
    }
}
