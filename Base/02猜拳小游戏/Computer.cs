﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02猜拳小游戏
{
    class Computer
    {
        public string FistName { get; set; }  // 定义 自动属性

        // 电脑出拳：随机产生结果，不需要外界传递参数，这点和 用户类 是 不同的
        public int ShowFist()
        {
            int res = 0;
            Random random = new Random();

            res = random.Next(1, 4); // 生成 1 2 3 的随机数  分别 代表 石头 剪刀 布 

            switch (res)
            {
                case 1:
                    FistName = "石头";
                    break;
                case 2:
                    FistName = "剪刀";
                    break;
                case 3:
                    FistName = "布";
                    break;

                default:
                    break;
            }

            return res; // 返回生成的随机数即可，代表序号
        }
    }
}
