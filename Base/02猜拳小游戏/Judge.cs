﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02猜拳小游戏
{
    class Judge
    {
        // -1  2  是可以赢的
        public string JudgeRes(int user, int computer)
        {
            // 石头1   剪刀2   布3
            // 1-2 = -1    2-3 = -1    3-1 = 2

            if (user - computer == -1 || user - computer == 2)
            {
                return "用户赢了!";
            }
            else if (user - computer == 0)
            {
                return "—平局—";
            }
            else
            {
                return "用户输了";
            }
        }

    }
}
