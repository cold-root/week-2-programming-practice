﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02猜拳小游戏
{
    class User
    {
        // 定义用户 保存 用户 出拳结果的属性

        public string FistName { get; set; }

        // 定义 方法 用户 描述用户出拳
        // 用整数 1 2 3 来表示 出拳结果
        // 接收的是按钮上的一个文本内容(也就是 按钮的名字，下面这个函数 可以 将 这个名字 转成 对应的整数)
        public int ShowFist(string msg)
        {
            FistName = msg;
            int res = 0;

            switch (msg)
            {
                case "石头":
                    res = 1;
                    break;
                case "剪刀":
                    res = 2;
                    break;
                case "布":
                    res = 3;
                    break;
                default:
                    break;
            }

            return res;
        }


    }
}
