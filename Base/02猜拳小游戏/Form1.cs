﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02猜拳小游戏
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // 多态思想：三个按钮 共用一套代码
        private void button_judge(object sender, EventArgs e)
        {
            // sender代表当前按钮   e 代表一些参数信息
            // sender是Object类型，可以强制转换为Button类型
            User user = new User();
            int userplayer = user.ShowFist( ((Button)sender).Text );
            //int userplayer = user.ShowFist(button1.Text);
            // 点击button1(Text是石头)，将 "石头" 传入，user.FistName = msg;
            // 然后 user.ShowFist("石头")函数 会 返回石头对应的序号：1

            lbl_peo.Text = user.FistName; // 将用户标签置为 "石头"。

            Computer computer = new Computer();
            int computerplayer = computer.ShowFist();  // 得到电脑随机产生的序号
            lbl_com.Text = computer.FistName;

            Judge judge = new Judge();
            lbl_Res.Text = judge.JudgeRes(userplayer, computerplayer);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            User user = new User();
            int userplayer = user.ShowFist(button1.Text);
            // 点击button1(Text是石头)，将 "石头" 传入，user.FistName = msg;
            // 然后 user.ShowFist("石头")函数 会 返回石头对应的序号：1

            lbl_peo.Text = user.FistName; // 将用户标签置为 "石头"。

            Computer computer = new Computer();
            int computerplayer = computer.ShowFist();  // 得到电脑随机产生的序号
            lbl_com.Text = computer.FistName;

            Judge judge = new Judge();
            lbl_Res.Text = judge.JudgeRes(userplayer, computerplayer);
  
        }
    }
}
