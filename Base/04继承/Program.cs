﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _04继承
{
    class Program
    {
        static void Main(string[] args)
        {
            Bird[] birds = {
                //new Bird(); // 抽象类 不可以进行实例化
                new Magpie(),
                new Yanzi()
            };

            foreach (Bird bird in birds)
            {
                bird.Eat();
                bird.test();
            }

            Console.ReadKey();
        }
    }

    public abstract class Bird
    {
        // 抽象类，并不是所有的方法 都是 abstract的
        //public virtual void Eat()
        //{
            //Console.WriteLine("喜鹊");    // 虚拟方法，可以有方法体，可以被调用  也可以被子类重写
        //}

        public abstract void Eat();     // 抽象方法，不可以有方法体，可以被子类重写

        public void test()  // 抽象类中 可以 有 非抽象方法，是可以被调用的
        {
            Console.WriteLine("test函数，不是abstract的");
        }

    }

    public class Magpie:Bird
    {
        public override void Eat()
        {
            Console.WriteLine("喜鹊");
        }
    }

    public class Yanzi : Bird
    {
        public override void Eat()
        {
            Console.WriteLine("燕子");
        }
    }


}
