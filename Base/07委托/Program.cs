﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07委托
{
    class Program
    {
        static void Main(string[] args)
        {
            // 委托就是 对方法的 引用 ，它也是一种数据类型
            MyDelegate1 myDelegate1 = func1;    // 直接把 方法名 赋值 给 委托元素

            myDelegate1 += func2;
            myDelegate1 += func3;

            myDelegate1();  // 直接调用委托元素  若同时关联多个方法，一次调用 可以 同时执行多个方法
            Console.ReadKey();


        }

        // 定义了三个 无参无返回值的方法 
        static void func1()
        {
            Console.WriteLine("委托测试，委托元素func1");
        }
        static void func2()
        {
            Console.WriteLine("委托测试，委托元素func2");
        }
        static void func3()
        {
            Console.WriteLine("委托测试，委托元素func2");
        }

        static int func_int_1(int a)
        {
            Console.WriteLine("委托测试，委托元素func1");
            return 1 * a;
        }
        static int func_int_2(int a)
        {
            Console.WriteLine("委托测试，委托元素func2");
            return 2 * a;

        }
        static int func_int_3(int a)
        {
            Console.WriteLine("委托测试，委托元素func2");
            return 3 * a;

        }
    }

    // 委托 Delegate 和 Class 是 同级别的    委托是一种类型，定义好了之后 就可以去声明对象了
    public delegate void MyDelegate1(); // 对应于func1，也要是 无参 无返回值的

    // 委托的作用：关联方法

    public delegate int MyDelegate2(int a);


}
