﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06鸭子案例
{
    class Program
    {
        static void Main(string[] args)
        {
            // 创建三类鸭子，他们都会游泳
            Duck[] ducks = {
                new EraserDuck(),
                new WoodDuck(),
                new RealDuck()
            };
            foreach (Duck duck in ducks)
            {
                duck.Swim();
            }

            // 创建会叫的两类鸭子
            IShout[] iShouts =
            {
                new EraserDuck(),
                new RealDuck(),
            };
            foreach (IShout iShout in iShouts)
            {
                iShout.shot();
            }
            Console.WriteLine("木头鸭子不会叫");

            Console.ReadKey();
        }
    }

    public abstract class Duck {
        public abstract void Swim();
    }

    public interface IShout
    {
        void shot();
    }

    public class EraserDuck : Duck, IShout
    {
        public override void Swim()
        {
            Console.WriteLine("橡皮鸭子会游泳");
        }

        public void shot()
        {
            Console.WriteLine("橡皮鸭子“唧唧叫”");
        }
    }

    public class WoodDuck : Duck
    {
        public override void Swim()
        {
            Console.WriteLine("木头鸭子会游泳");
        }
    }

    public class RealDuck : Duck, IShout
    {
        public override void Swim()
        {
            Console.WriteLine("真实鸭子会游泳");
        }

        public void shot()
        {
            Console.WriteLine("真实鸭子“嘎嘎叫”");
        }
    }
}
