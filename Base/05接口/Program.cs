﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05接口
{
    class Program
    {
        static void Main(string[] args)
        {

            MyAchieve achi1 = new MyAchieve();
            Console.WriteLine(achi1.Method2(1000000000, 100000));

            IFlyable[] flyables = {
                new Magpie(),
                new Yanzi(),
                new Plane()
            };

            foreach (var item in flyables)
            {
                item.fly();
            }

            Console.ReadKey();
        }
    }

    public interface MyInterface
    {
        // 接口里面 所有的 方法(包括接口) 都是抽象的    所有方法：public + abstract
        // 子类，只能继承 一个父类，但是可以去实现多个接口。 因此可以有选择性的去实现一些接口
        //public int Method1(int var1, long var2); // 接口的成员是隐式public的
        int Method2(int var1, long var2);
    }

    public class MyAchieve : MyInterface
    {
        public int Method2(int var1, long var2)
        {
            Console.WriteLine("var1:{0}, var2:{1}", var1, var2);
            return var1 + (int)var2;

        }
    }

    public abstract class Bird  // 鸟类父类
    {
        public abstract void Eat();     // 抽象方法，不可以有方法体，可以被子类重写
    }

    public interface IFlyable
    {
        void fly(); // 所有方法：public + abstract
    }


    public class Magpie : Bird, IFlyable
    {
        public override void Eat()
        {
            Console.WriteLine("喜鹊");
        }

        public void fly()
        {
            Console.WriteLine("喜鹊会飞");
        }
    }

    public class Yanzi : Bird, IFlyable
    {
        public override void Eat()
        {
            Console.WriteLine("燕子");
        }

        public void fly()
        {
            Console.WriteLine("燕子会飞");
        }
    }

    public class Penguin : Bird
    {
        public override void Eat()
        {
            Console.WriteLine("燕子");
        }
    }

    public class Plane : IFlyable
    {
        public void fly()
        {
            Console.WriteLine("飞机会飞");

        }
    }

}
