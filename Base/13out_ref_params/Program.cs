﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace _13out_ref_params
{
    class Program
    {
        public void refMethod(ref int value)
        {
            value *= value;
        }

        // out主要用于返回多个值
        // 在方法体内部 必须对out修饰的参数赋值
        public int OutMethod(out int value)
        {
            value = 10; // 必须要对 out修饰的参数 赋值
            return 1;
        }

        /// <summary>
        /// params用于声名可变参数
        /// </summary>
        public int ParamsMethod(params int[] values)
        {
            int total = 0;
            foreach (var item in values)
            {
                total += item;
            }
            return total;
        }

        static void Main(string[] args)
        {
            Program test = new Program();

            #region 1.ref
            // ref用于 将值传递 转为 引用传递，ref修饰的参数 必须 事先赋值
            int value = 10;
            Console.WriteLine("调用前的value：" + value);
            test.refMethod(ref value);
            Console.WriteLine("调用后的value：" + value);

            #endregion

            #region 2. out
            // out主要用于返回多个值
            int outInt;
            int outInt2 = test.OutMethod(out outInt);
            Console.WriteLine("outInt 1:" + outInt);
            Console.WriteLine("outInt 2:" + outInt2);

            #endregion


            #region 3. params
            // params 用于声明可变参数
            Console.WriteLine("this is params test -------");
            int paramsInt = test.ParamsMethod(1, 2, 3, 4);
            Console.WriteLine("paramsInt :" + paramsInt);
            Console.ReadKey();

            #endregion
            Console.ReadKey();
        }
    }
}
