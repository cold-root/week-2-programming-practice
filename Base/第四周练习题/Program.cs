﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 第四周练习题
{
    class Program
    {

        static void Main(string[] args)
        {
            ArrayList list1 = new ArrayList();

            // 添加集合元素
            list1.Add(100);
            list1.Add(3.14);
            list1.Add("hello");
            list1.Add(true);

            int[] array = { 1, 2, 3, 4, 5 };
            list1.AddRange(array);
            list1.RemoveAt(0);  // 将索引为0的删掉
            list1.RemoveRange(4, 2); // 从索引为4开始，删除两个元素

            // 插入集合元素
            // 默认是添加到集合的尾部
            list1.Insert(1, "你好");
            int[] arr2 = { 22, 222, 333 };
            list1.InsertRange(2, arr2);

            // 修改集合元素
            list1[1] = "你不好";

            // 删除集合元素
            list1.Remove("hello");

            // 清楚所有元素
            list1.Clear();

            Console.WriteLine("集合中元素的个数为" + list1.Count);
            for (int i = 0; i < list1.Count; i++)
            {
                Console.WriteLine(list1[i]);
            }
            Console.ReadKey();

            #region 2.某int集合中有10个不重复的随机整数，将其中的奇数挑选出来，保存在一个int数组中。（提示：List<T>）

            // List<T>类解释：
            /* List<T>类是 ArrayList 类的泛型等效类。该类使用大小可按需动态增加的数组实现 IList<T> 泛型接口。
             * 
             * 泛型的好处：
             * 它为使用C#语言编写 面向对象程序 增加了极大的效力 和 灵活性。
             * 不会强行对值类型进行装箱和拆箱，或对引用类型进行，向下强制类型转换，所以性能得到提高。
             */

            #endregion
            List<int> list = new List<int>() { 1, 2, 22, 14, 55, 20, 30, 4, 7, 10 };

        }
    }
}
