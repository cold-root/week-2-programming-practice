﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12事件
{
    class Program
    {
        static void Main(string[] args)
        {
            // 事件 是 当对象发生某些动作 或 事情时，向其他对象提供消息的 一种方式
            // 在C#中，事件 通过 委托来实现
            // 事件一般 关联两个对象：触发对象 和 发送对象

            // 事件是 类的 方法成员之一，与 委托紧密关联

            // 为了使 委托对象 具有 与 成员方法 相同的地位，需要在委托对象前 加上关键字 event，
            // 这样委托对象 就 称为了 事件


            SchoolRing schoolRing = new SchoolRing();
            Student student = new Student();
            schoolRing.myEvent += student.Prepare;

            schoolRing.Ring("上课铃声");
            // 调用 可触发事件的函数， 然后去 触发那个事件
            // 事件里面已经 加入了一个函数：就是 student.Prepare
            // 最后的效果就是 会去 调用这个 注册的委托方法 Prepare("上课铃声")

            Console.ReadKey();
        }
    }

    // 声明委托类型
    public delegate void MyDelegate(string msg);

    // 声明事件的发布者
    class SchoolRing
    {
        public event MyDelegate myEvent;    // 这个 myEvent就是事件

        // 自定义方法 用于 触发事件
        public void Ring(string msg)
        {
            myEvent(msg);
        }
    }

    // 声明事件的接受者
    class Student
    {
        public void Prepare(string str)
        {
            Console.WriteLine("听到{0}，同学们要准备上课了。", str);
        }
    }

}
